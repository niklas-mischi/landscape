package at.nmis.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class HTLRectangle implements Actor {
	private double x, y, degree, SPEED;
	private int width, height;
	public Shape shape;
	public boolean rectRight = true;
	public boolean rectLeft = false;

	public HTLRectangle(double x, double y, double degree, int width, int height, double SPEED) {
		super();
		this.x = x;
		this.y = y;
		this.degree = degree;
		this.width = width;
		this.height = height;
		this.SPEED = SPEED;
		this.shape = new Rectangle((float) this.x, (float) this.y, this.width, this.height);
	}

	public void update(GameContainer gc, int delta) {
		if (x >= 600) {
			rectRight = false;
			rectLeft = true;
		} else if (x <= 100) {
			rectRight = true;
			rectLeft = false;
		}

		if (rectLeft == true) {
			this.x -= delta * SPEED;
		} else if (rectRight = true) {
			this.x += delta * SPEED;
		}

		shape.setX((float) this.x);
		shape.setY((float) this.y);
	}

	public void render(Graphics graphics) {
		graphics.drawRect((int) this.x, (int) this.y, this.width, this.height);
		graphics.draw(shape);
	}

	public Shape getShape() {
		return shape;
	}

}
