package at.nmis.landscape;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Bullet implements Actor {

	private double x, y;
	private Image image;
	public Shape shape;

	public Bullet(double x, double y) {
		super();
		this.x = x;
		this.y = y;

		try {
			image = new Image("player/root2.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

//			this.shape = new Rectangle((float) this.x, (float) this.y, 40, 40);
		}
	}

	@Override
	public void render(Graphics graphics) {
//		graphics.setColor(Color.blue);
//		graphics.fillOval((float) this.x + 18, (float) this.y + 59, 10, 10);
//		graphics.setColor(Color.white);

		image.draw((int) this.x + 18, (int) this.y + 55, 40, 40);
 
	}

	@Override
	public void update(GameContainer gc, int delta) {
		this.x++;

//		shape.setX((float) this.x);
	}

}
