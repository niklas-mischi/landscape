package at.nmis.landscape;

import java.util.List;
import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Shape;

public class Player implements Actor {

	private double x;
	private double y;
	private Shape shape;
	private List<Shape> collisionPartners;
	private Image image;

	public Player(double x, double y) {
		super();
		this.collisionPartners = new ArrayList<>();
		this.x = x;
		this.y = y;
		this.shape = new org.newdawn.slick.geom.Rectangle((float) this.x + 30, (float) this.y + 30, 20, 20);
		
		try {
			image = new Image("player/Auge.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void addCollisonPartner(Shape s) {
		this.collisionPartners.add(s);
	}
	@Override
	public void render(Graphics graphics) {
//		graphics.fillRect((int) this.x, (int) this.y, 20, 20);
		graphics.draw(shape);
		image.draw((int)this.x, (int)this.y, 64, 90);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		if (gc.getInput().isKeyDown(Input.KEY_DOWN) || gc.getInput().isKeyDown(Input.KEY_S)) {
			y += delta * 0.5;
		} else if (gc.getInput().isKeyDown(Input.KEY_UP) || gc.getInput().isKeyDown(Input.KEY_W)) {
			y -= delta * 0.5;
		} else if (gc.getInput().isKeyDown(Input.KEY_RIGHT) || gc.getInput().isKeyDown(Input.KEY_D)) {
			x += delta * 0.5;
		} else if (gc.getInput().isKeyDown(Input.KEY_LEFT) || gc.getInput().isKeyDown(Input.KEY_A)) {
			x -= delta * 0.5;
		}

		shape.setX((float) this.x + 30);
		shape.setY((float) this.y + 30);
		
//		this.collisionPartners.add(HTLRectangle);

		for (Shape s : collisionPartners) {
			if (s.intersects(this.shape)) {
				System.out.println("Collision");
			}
		}

	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}
