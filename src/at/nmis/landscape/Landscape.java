package at.nmis.landscape;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AngelCodeFont;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.opengl.pbuffer.GraphicsFactory;

public class Landscape extends BasicGame {

	private List<Actor> actor;
	private Player player1;


	public Landscape(String title) {
		super(title);

	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		for (Actor actors : this.actor) {
			actors.render(graphics);
		}
	}

	@Override
	public void init(GameContainer gc) throws SlickException {

		this.actor = new ArrayList<>();
//		HTLRectangle rect = new HTLRectangle(100, 100, 1, 50, 50, 0.3);
//		this.actor.add(rect);
//		this.actor.add(new HTLCircle(100, 100, 1, 0.002, 50));
//		this.actor.add(new HTLOval(500, 50, 1, 0.2, 50, 70));

		for (int i = 0; i <= 70; i++) {
			this.actor.add(new HTLSnowflakes(30, 0.2));
			this.actor.add(new HTLSnowflakes(20, 0.15));
			this.actor.add(new HTLSnowflakes(10, 0.1));
		}
		


		player1 = new Player(100, 300);
		this.actor.add(this.player1);
//		this.player1.addCollisonPartner(rect.getShape());

	}

	@Override
	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		super.keyPressed(key, c);
		if (key == Input.KEY_SPACE) {
			Actor bullet = new Bullet(this.player1.getX(), this.player1.getY());
			this.actor.add(bullet);

		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		for (Actor actors : this.actor) {
			actors.update(gc, delta);
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Landscape("FirstGame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
