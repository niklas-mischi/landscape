package at.nmis.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import java.util.Random;

public class HTLSnowflakes implements Actor {
	private double speed;
	private int radius;
	private double x;
	private double y;
	private Image image;

	public HTLSnowflakes(int radius, double SPEED) {
		super();
		this.radius = radius;
		this.speed = SPEED;

		setRandomPosition();
		
		try {
			image = new Image("player/snowflake.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void setRandomPosition() {
		x = (int) (Math.random() * 800);
		y = (int) (Math.random() * -600);

	}

	@Override
	public void render(Graphics graphics) {

//		graphics.fillOval((int) this.x, (int) this.y, this.radius, this.radius);
		
		image.draw((int)this.x, (int)this.y, this.radius, this.radius);

	}

	@Override
	public void update(GameContainer gc, int delta) {
		this.y += delta * speed;
		if (y >= 700) {
		setRandomPosition();
		}
	}
}
