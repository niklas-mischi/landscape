package at.nmis.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLOval implements Actor{
	private double x, y, degree, SPEED;
	private int height, width;

	public HTLOval(double x, double y, double degree, double SPEED, int length, int width) {
		super();
		this.x = x;
		this.y = y;
		this.degree = degree;
		this.SPEED = SPEED;
		this.height = length;
		this.width = width;
	}

	public void update(GameContainer gc, int delta) {
		this.y += delta * SPEED;
		if (y >= 700) {
			this.y = 0;
		}
	}

	public void render(Graphics graphics) {
		graphics.drawOval((int) this.x, (int) this.y, this.width, this.height);

	}

}
