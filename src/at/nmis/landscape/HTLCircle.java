package at.nmis.landscape;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class HTLCircle implements Actor {
	private double x, y, degree, SPEED;
	private int radius;

	public HTLCircle(double x, double y, double degree, double SPEED, int radius) {
		super();
		this.x = x;
		this.y = y;
		this.degree = degree;
		this.SPEED = SPEED;
		this.radius = radius;

	}

	public void update(GameContainer gc, int delta) {
		this.x = (int) (200 + 200 * Math.sin(this.degree += delta * SPEED));
		this.y = (int) (200 + 200 * Math.cos(this.degree += delta * SPEED));
	}

	public void render(Graphics graphics) {
		graphics.drawOval((int) this.x, (int) this.y, this.radius, this.radius);
	}

}
