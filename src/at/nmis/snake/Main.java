package at.nmis.snake;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class Main extends BasicGame {

	private List<Actor> actor;

	public Main(String title) {
		super(title);
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actors : this.actor) {
			actors.render(graphics);
		}
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		// TODO Auto-generated method stub
		this.actor = new ArrayList<>();
		this.actor.add(new Snake(100, 100, 0.2, 15));
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// TODO Auto-generated method stub
		for (Actor actors : this.actor) {
			actors.update(gc, delta);
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new Main("Snake"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
