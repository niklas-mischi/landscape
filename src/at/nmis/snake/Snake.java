package at.nmis.snake;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.util.Log;

public class Snake implements Actor{

	private double x, y, SPEED;
	private int radius;
	private int status;

	public Snake(double x, double y, double speed, int radius) {
		this.x = x;
		this.y = y;
		SPEED = speed;
		this.radius = radius;

	}

	public void render(Graphics graphics) {
		graphics.fillOval((float) this.x, (float) this.y, this.radius, this.radius);
	}

	@Override
	public void update(GameContainer gc, int delta) {
		// TODO Auto-generated method stub
		if (gc.getInput().isKeyPressed(Input.KEY_DOWN) || gc.getInput().isKeyDown(Input.KEY_S)) {
			this.status = 1;

		} else if (gc.getInput().isKeyPressed(Input.KEY_UP) || gc.getInput().isKeyDown(Input.KEY_W)) {
			this.status = 2;
		} else if (gc.getInput().isKeyPressed(Input.KEY_RIGHT) || gc.getInput().isKeyDown(Input.KEY_D)) {
			this.status = 3;
		} else if (gc.getInput().isKeyPressed(Input.KEY_LEFT) || gc.getInput().isKeyDown(Input.KEY_A)) {
			this.status = 4;
		}

		if (status == 1) {
			y += delta * this.SPEED;
		} else if (status == 2) {
			y -= delta * this.SPEED;
		} else if (status == 3) {
			x += delta * this.SPEED;
		} else if (status == 4) {
			x -= delta * this.SPEED;
		}

		if (x > 800) {
			x = 0;
		} else if (x < 0) {
			x = 800;
		} else if (y > 600) {
			y = 0;
		} else if (y < 0) {
			y = 600;
		}
	}

}
